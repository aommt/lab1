# import numpy as np
import time
from math import radians



from config import testFile, grayImage, cmykImage, hsvImage, yuvImage
from files import readFileToList, listToWriteFile
from plot import printHistogram

def convert_bgr_2_grayscale(list):
    newlist = []
    newlist.append("P2\n# \n")
    newlist.append(list[2]+"\n255\n")

    for i in range(int(len(list)/3)-1):
        y = 0.299* int(list[i*3+4])+ 0.587*int(list[i*3+5]) + 0.114*int(list[i*3+6])
        newlist.append(str(int(y)))
    return  newlist


def convert_bgr_2_hsv(list):
    newlist = []
    # newlist.append("P3\n#\n") # не нужно?
    # newlist.append(list[2] + "\n") # не нужно?
    # newlist.append("360" + "\n")  # не нужно?

    # gimp никак не мог понять, что это не rgb (выводил очень красную картинку))
    # при необходимости верхние строки раскомментировать.

    # сейчас будет просто набор данных, без параметризации под изображение,
    # формат и имя получаемого файла задается в config.py

    for i in range(int(len(list) / 3) - 1):

        R = int(list[i * 3 + 4]) / 255
        G = int(list[i * 3 + 5]) / 255
        B = int(list[i * 3 + 6]) / 255
        C_max = max(R, G, B)
        C_min = min(R, G, B)
        delta = C_max - C_min
        if delta == 0:
            H = 0
        elif C_max == R:
            H = 60 * (((G - B) / delta) % 6)
        elif C_max == G:
            H = 60 * (((B - R) / delta) + 2)
        else:
            H = 60 * (((R - G) / delta) + 4)
        if C_max == 0:
            S = 0
        else:
            S = delta / C_max
        V = C_max

        newlist.append(str((H)))
        newlist.append(str((S)))
        newlist.append(str((V)))

    return newlist


def convert_bgr_2_yuv(list):
    newlist = []
    # newlist.append("P3\n#\n")
    #
    # newlist.append(list[2] + "\n")
    # newlist.append("255" + "\n")

    # gimp никак не мог понять, что это не rgb.
    # при необходимости верхние строки раскомментировать, получается светлорозоватое изображение
    # сейчас будет просто набор данных, без параметризации под изображение,
    # формат и имя получаемого файла задается в config.py

    for i in range(int(len(list) / 3) - 1):
        R = int(list[i * 3 + 4])
        G= int(list[i * 3 + 5])
        B= int(list[i * 3 + 6])

        Y = 0.257 * R + 0.504 * G + 0.098 * B + 16
        U = -0.148 * R - 0.291 * G + 0.439 * B + 128
        V = 0.439 * R - 0.368 * G - 0.071 * B + 128

        newlist.append(str((Y)))
        newlist.append(str((U)))
        newlist.append(str((V)))
    return newlist


# Опционально
def convert_bgr_2_cmyk(list):
    newlist = []
    # newlist.append("P3\n#\n")
    # newlist.append(list[2] + "\n")
    # newlist.append("255" + "\n")

    for i in range(int(len(list) / 3) - 1):
        R = int(list[i * 3 + 4]) / 255
        G = int(list[i * 3 + 5]) / 255
        B = int(list[i * 3 + 6]) / 255
        K = 1 - max(R, G, B)
        C = (1 - R - K) / (1 - K)
        M = (1 - G - K) / (1 - K)
        Y = (1 - B - K) / (1 - K)

        newlist.append(str((K)))
        newlist.append(str((C)))
        newlist.append(str((M)))
        newlist.append(str((Y)))

    return newlist

def getR(list):
    newlistR = []
    for i in range(int(len(list) / 3) - 1):
        newlistR.append(int(list[i * 3 + 4]))

    newlistR.sort()
    return newlistR

def getG(list):
    newlistG = []
    for i in range(int(len(list) / 3) - 1):
        newlistG.append(int(list[i * 3 + 5]))
    newlistG.sort()
    return newlistG

def getB(list):
    newlistB = []
    for i in range(int(len(list) / 3) - 1):
        newlistB.append(int(list[i * 3 + 6]))
    newlistB.sort()
    return newlistB




def main():
    # Чтение изображения ppm (путь до testFile прописан в config.py)

    lst = readFileToList(testFile)

    # построение гистограмм
    printHistogram(getR(lst), 'red')
    printHistogram(getG(lst), 'green')
    printHistogram(getB(lst), 'blue')

    # преобразования
    glst = convert_bgr_2_grayscale(lst)
    hsvlst = convert_bgr_2_hsv(lst)
    yuvlst = convert_bgr_2_yuv(lst)
    cmyklst = convert_bgr_2_cmyk(lst)

    # запись в файлы
    listToWriteFile(glst, grayImage)
    listToWriteFile(hsvlst, hsvImage)
    listToWriteFile(yuvlst, yuvImage)
    listToWriteFile(cmyklst, cmykImage)




if __name__ == "__main__":
    main()