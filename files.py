def readFileToList(readFile):
    try:
        img = open(readFile, 'r')
        lst = list()
        for line in img:
            lst.extend(line.rstrip().split('\n'))

        # if (lst[0] != "P3"):
        #     return NotImplementedError

        return lst

    except IOError:
        print("An IOError has occurred!")
    finally:
        img.close()



def listToWriteFile(list, writeFile):
    img2 = open(writeFile, 'w')
    img2.write('\n'.join(list) + '\n')
    img2.close()
